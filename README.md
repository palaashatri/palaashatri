Currently part of the SDET-QA team for Azul Zing (Platform Prime) and CNVM. 

Previously, Engineer at L&T Technology Services, working as a Backend Engineer for Halliburton on DecisionSpace (DS365.ai) Platform team, building Java SDKs, microservices and Kubernetes automation, validation and deployment tools using Python.

I speak C++, Java, JavaScript & Python 3.

I prefer Windows, but I work on macOS and Linux. The only Linux distro I prefer working on is Arch btw.

You can find me on LinkedIn at: https://www.linkedin.com/in/palaasha/